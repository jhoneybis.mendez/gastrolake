#!/usr/bin/env python
# -*- coding: utf-8 -*-
import unittest
import main


class TestLogin(unittest.TestCase):
    def test_sucess_conex(self):
        self.assertEqual(main.auth().status_code, 200, "Fail to connect")
        self.assertEqual(
            main.auth().json()["resultCode"], "SUCCESS", "Api Error"
        )


if __name__ == "__main__":
    unittest.main()
