#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dotenv import load_dotenv

from datetime import date, timedelta, datetime
import os
import requests
import json
import pandas as pd
import logging


logger = logging.getLogger()
logger.setLevel(logging.INFO)

env_path = ".env"
load_dotenv(dotenv_path=env_path)
headers = {"Content-Type": "application/json"}


def separated_dates(date):
    try:
        date = datetime.strptime(date, "%Y-%m-%d")
        m = date.month
        y = date.year
        d = date.day
    except ValueError as e:
        logger.error(f"Error to try separate dates -> {e}")
        raise ValueError("Date format error. The correct format is -> 'Y-m-d'")

    return m, y, d


def create_files(file_name, data, type) -> None:

    logging.info("Build .json file...")

    with open(f"{file_name}.json", "a", encoding="utf-8") as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

    logging.info("Build Succesfully -> %s" % f"{file_name}.json")

    if type == 1:
        logging.info("Build .csv file...")

        df = pd.DataFrame(data)
        if os.path.isfile(f"{file_name}.csv"):
            df.to_csv(f"{file_name}.csv", mode="a", header=False)
        else:
            df.to_csv(f"{file_name}.csv")

        logging.info("Build Succesfully -> %s" % f"{file_name}.csv")


def days_between(date_start, date_end):
    if date_end < date_start:
        raise TypeError("Error in dates")

    m_start, y_start, d_start = separated_dates(date_start)
    m_end, y_end, d_end = separated_dates(date_end)

    d_start = date(y_start, m_start, d_start)
    d_end = date(y_end, m_end, d_end)
    delta = d_end - d_start

    return [
        (d_start + timedelta(days=i)).strftime("%Y-%m-%d")
        for i in range(delta.days + 1)
    ]


def auth():
    global headers

    endpoint = "https://api-mconn.maxisistemas.com.ar/login"
    email = os.environ["EMAIL"]
    password = os.environ["PASS"]
    cod_cli = int(os.environ["COD_CLI"])
    logging.info("Try to connect...")
    params = {"email": email, "pass": password, "cod_cli": cod_cli}
    response = requests.post(endpoint, json=params, headers=headers)
    token = response.json()["content"]["tokenAccess"]
    headers["Authorization"] = f"Bearer {token}"
    logging.info("Conection Status -> %s" % (response.json()["resultCode"]))
    return response


def marketplace_stadistics(date, end_file) -> None:
    endpoint = (
        "https://api-mconn.maxisistemas.com.ar/estadisticaspedidos/partners"
    )
    params = {"fecha": date}

    response = requests.get(endpoint, json=params, headers=headers)
    data = response.json()["content"]["content"]
    file_name = f"./data/marketplace/data_{end_file}"
    create_files(file_name, data, type=1)


def accounting(date_start, date_end, end_file):
    endpoint = (
        "https://api-mconn.maxisistemas.com.ar/estadisticas/fullinvoicebydate"
    )
    params = {"from": date_start, "to": date_end}

    response = requests.get(endpoint, params=params, headers=headers)
    data = response.json()["content"]["content"]
    file_name = f"./data/accounting/data_{end_file}"
    create_files(file_name, data, type=2)


def main():
    response = auth().json()["resultCode"]
    if response == "SUCCESS":
        end_file = "2021-08-1_2021-08-15"
        days = days_between("2021-08-1", "2021-08-15")
        accounting("2021-08-1", "2021-08-08", end_file)
        for day in days:
            marketplace_stadistics(day, end_file)
    else:
        raise TypeError("Error in conection")


if __name__ == "__main__":
    main()
